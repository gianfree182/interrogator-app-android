package frank.free.interrogator;

import android.content.ClipData;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;

public class AdapterEditQuestion extends BaseAdapter {

    private Context context;
    private ArrayList<Pair<String, String>> listQuestion;
    LayoutInflater inflater;

    public AdapterEditQuestion(Context context, ArrayList<Pair<String, String>> listQuestion, Integer number) {
        this.context = context;
        this.listQuestion = listQuestion;
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return listQuestion.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return listQuestion.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void remove (int position) {
        listQuestion.remove(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.item_edit_listview, parent, false);
        }

        TextView numberQuestion = (TextView) convertView.findViewById(R.id.textNumber);
        EditText domandaText = (EditText) convertView.findViewById(R.id.editTextDomanda);
        EditText rispostaText= (EditText) convertView.findViewById(R.id.editTextRisposta);
        Button deleteButton = (Button) convertView.findViewById(R.id.buttonRemove);

        deleteButton.setFocusable(false);

        deleteButton.setOnClickListener(v-> {
            remove(position);
            this.notifyDataSetChanged();
        });
        domandaText.setText(listQuestion.get(position).first);
        rispostaText.setText(listQuestion.get(position).second);
        numberQuestion.setText("#" + String.valueOf(position+1));

        return convertView;
    }
}
