package frank.free.interrogator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import frank.free.interrogator.Database.InterrogatorDbHelper;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class SelectEditActivity extends AppCompatActivity {

    private ListView listView;
    private InterrogatorDbHelper databaseSQL;
    private List<String> listOfId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_edit);
        this.listOfId = new ArrayList<>();

        listView = (ListView)findViewById(R.id.listView);
        databaseSQL = new InterrogatorDbHelper(this);

        SQLiteDatabase db = this.openOrCreateDatabase("Interrogator", MODE_PRIVATE, null);
        populateListView();

        if(listOfId.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Attenzione!");
            builder.setIcon(R.drawable.excla_image);
            builder.setMessage("Non hai creato ancora nessuna interrogazione! Cosa stai aspettando? Vuoi andare a crearne una?");
            builder.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(SelectEditActivity.this, CreateActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    SelectEditActivity.super.onBackPressed();
                }
            });
            builder.show();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                String value = (String)adapter.getItemAtPosition(position);
                Intent i = new Intent(v.getContext(), EditActivity.class);
                i.putExtra("interrogazione", value);
                i.putExtra("id", listOfId.get(position));
                startActivity(i);
            }
        });

    }

    private void populateListView() {
        Cursor data = databaseSQL.selectInterrogazioni();
        ArrayList<String> listData = new ArrayList<>();
        while(data.moveToNext()) {
            listData.add(data.getString(1));
            listOfId.add(String.valueOf(data.getInt(0)));
        }

        ListAdapter adapter = new ArrayAdapter<>(this, R.layout.item_create_listview, listData);
        listView.setAdapter(adapter);
    }

}
