package frank.free.interrogator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private Button buttonStart;
    private Button buttonCreate;
    private Button buttonEdit;
    private Button buttonInfo;
    private TextToSpeech mTTS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStart = (Button) findViewById(R.id.buttonStart);
        Intent intent = new Intent(this, ChooseActivity.class);
        buttonStart.setOnClickListener(v-> {
            startActivity(intent);
            mTTS.stop();
        });

        buttonCreate = (Button) findViewById(R.id.buttonCreate);
        Intent intentCreate = new Intent(this, CreateActivity.class);
        buttonCreate.setOnClickListener(v-> {
            startActivity(intentCreate);
            mTTS.stop();
        });

        buttonEdit = (Button) findViewById(R.id.buttonEdit);
        Intent intentEdit = new Intent(this, SelectEditActivity.class);
        buttonEdit.setOnClickListener(v->{
            startActivity(intentEdit);
            mTTS.stop();
        });

        checkIsReadLoud();
        buttonInfo = (Button) findViewById(R.id.buttonInfo);
        this.buttonInfo.setOnClickListener(v -> {
            if(mTTS.isSpeaking()) {
                mTTS.stop();
            } else {
                speak();
            }

        });
    }

    private void checkIsReadLoud() {

        mTTS = new TextToSpeech(this, new TextToSpeech.OnInitListener(){
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTTS.setLanguage(Locale.ITALIAN);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Language not supported");
                    } else {
                        //read.setEnabled(true);  si potrebbe disabilitare lo switch se non compatibile
                    }
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            }
        });
    }

    private void speak() {
        String text = "Ciao! Benvenuto su interroghetor, l'app che ti permette di creare, modificare, " +
                "un set di domande e risposte per poi farti interrogare. Buona interrogazione! ";
        /*-float pitch = (float) mSeekBarPitch.getProgress() / 50;
        if (pitch < 0.1) pitch = 0.1f;
        float speed = (float) mSeekBarSpeed.getProgress() / 50;
        if (speed < 0.1) speed = 0.1f;

        mTTS.setPitch(pitch);
        mTTS.setSpeechRate(speed);*/

        mTTS.setPitch(0.7f);
        mTTS.setSpeechRate(0.9f);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTTS.speak(text, TextToSpeech.QUEUE_FLUSH,null,null);
        } else {
            mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }

    }
}
