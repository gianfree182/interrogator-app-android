package frank.free.interrogator.Database;

public class QuestionInSessionTable {

    private int id;
    private int id_domanda;
    private int id_sessione;
    private int feedback;

    public QuestionInSessionTable() {
    }

    public QuestionInSessionTable(int id_domanda, int id_sessione) {
        this.id_domanda = id_domanda;
        this.id_sessione = id_sessione;
        feedback = 0;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_domanda(int id_domanda) {
        this.id_domanda = id_domanda;
    }


    public void setId_sessione(int id_sessione) {
        this.id_domanda = id_domanda;
    }

    public void setFeedback(int feedback) {
        this.feedback = feedback;
    }

    public int getId() {
        return this.id;
    }

    public int getId_domanda() {
        return this.id_domanda;
    }

    public int getId_sessione() {
        return this.id_sessione;
    }

    public int getFeedback() {
        return this.feedback;
    }
}
