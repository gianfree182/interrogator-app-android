package frank.free.interrogator.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class InterrogatorDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "InterrogazioneDb.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TEXT_TYPE = " TEXT";

    private static final String REAL_TYPE = " REAL";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";

    public static final String TABLE_NAME_INTERROGAZIONE = "interrogazione";
    public static final String TABLE_NAME_DOMANDA= "domanda";
    public static final String TABLE_NAME_SESSIONE= "sessione";
    public static final String TABLE_NAME_DOMANDA_SESSIONE = "domanda_sessione";

    public static final String INTERROGAZIONE_ID = "id";
    public static final String INTERROGAZIONE_NOME = "nome";

    public static final String SESSIONE_ID = "id";
    public static final String SESSIONE_ID_INTERROGAZIONE = "id_interrogazione";
    public static final String SESSIONE_TIMEINIZIO = "timestampInizio";
    public static final String SESSIONE_TIMEFINE = "timestampFine";

    public static final String DOMANDA_ID = "id";
    public static final String DOMANDA_DOMANDA = "domanda";
    public static final String DOMANDA_RISPOSTA = "risposta";
    public static final String DOMANDA_ID_INTERROGAZIONE = "id_interrogazione";

    public static final String DOMANDA_SESSIONE_ID = "id";
    public static final String DOMANDA_SESSIONE_ID_DOMANDA = "id_domanda";
    public static final String DOMANDA_SESSIONE_ID_SESSIONE = "id_sessione";
    public static final String DOMANDA_SESSIONE_FEED = "feedback";

    public static String getTextType() {
        return TEXT_TYPE;
    }

    public static String getInterrogatorNome() {
        return INTERROGAZIONE_NOME;
    }

    public static String getTableNameInterrogazione() {
        return TABLE_NAME_INTERROGAZIONE;
    }

    public static String getID() {
        return INTERROGAZIONE_ID;
    }

    private static final String TABLE_INTERROGATOR_CREATE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME_INTERROGAZIONE + " (" + INTERROGAZIONE_ID + " INTEGER PRIMARY KEY  AUTOINCREMENT, "
            + INTERROGAZIONE_NOME + TEXT_TYPE + " )";

    private static final String TABLE_QUESTION_CREATE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME_DOMANDA + " (" + DOMANDA_ID + " INTEGER PRIMARY KEY  AUTOINCREMENT, "
            + DOMANDA_DOMANDA + TEXT_TYPE + COMMA_SEP + DOMANDA_RISPOSTA + TEXT_TYPE + COMMA_SEP + DOMANDA_ID_INTERROGAZIONE + INTEGER_TYPE + " )";

    private static final String TABLE_QUESTION_SESSION_CREATE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME_DOMANDA_SESSIONE + " (" + DOMANDA_SESSIONE_ID + " INTEGER PRIMARY KEY  AUTOINCREMENT, "
            + DOMANDA_SESSIONE_ID_DOMANDA + INTEGER_TYPE + COMMA_SEP + DOMANDA_SESSIONE_ID_SESSIONE
            + INTEGER_TYPE + COMMA_SEP + DOMANDA_SESSIONE_FEED + INTEGER_TYPE + " )";

    private static final String TABLE_SESSION_CREATE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME_SESSIONE + " (" + SESSIONE_ID + " INTEGER PRIMARY KEY  AUTOINCREMENT, "
            + SESSIONE_ID_INTERROGAZIONE + INTEGER_TYPE + COMMA_SEP + SESSIONE_TIMEINIZIO +
            TEXT_TYPE + COMMA_SEP + SESSIONE_TIMEFINE + TEXT_TYPE + " )";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_INTERROGATOR_CREATE);
        db.execSQL(TABLE_QUESTION_CREATE);
        db.execSQL(TABLE_SESSION_CREATE);
        db.execSQL(TABLE_QUESTION_SESSION_CREATE);
    }

    public InterrogatorDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public boolean insert(String nome) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(INTERROGAZIONE_NOME, nome);

        Log.d("SQL", "addData: Adding " + nome + " to " + TABLE_NAME_INTERROGAZIONE);

        final long result = db.insert(TABLE_NAME_INTERROGAZIONE, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean insertDomanda(String domanda, String risposta, int idInterrogazione) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DOMANDA_ID_INTERROGAZIONE, idInterrogazione);
        contentValues.put(DOMANDA_RISPOSTA, risposta);
        contentValues.put(DOMANDA_DOMANDA, domanda);

        final long result = db.insert(TABLE_NAME_DOMANDA, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }


    public Cursor selectInterrogazioni(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME_INTERROGAZIONE;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor selectDomandeFromId(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME_DOMANDA + " WHERE id_interrogazione = " + id;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor selectWhereName(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INTERROGAZIONE_ID + " FROM " + TABLE_NAME_INTERROGAZIONE +
                " WHERE " + INTERROGAZIONE_NOME + " = '" + name + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor selectWhereId(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INTERROGAZIONE_NOME + " FROM " + TABLE_NAME_INTERROGAZIONE +
                " WHERE " + INTERROGAZIONE_ID + " = '" + id + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }


    public String getIdFromNameInterrogazione(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INTERROGAZIONE_ID + " FROM " + TABLE_NAME_INTERROGAZIONE +
                " WHERE " + INTERROGAZIONE_NOME + " = '" + name + "'";
        Cursor data = db.rawQuery(query, null);
        data.moveToFirst();
        return data.getString(data.getColumnIndex("id"));
    }

    public String getHighterId() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INTERROGAZIONE_ID + " FROM " + TABLE_NAME_INTERROGAZIONE +
                " ORDER BY " + INTERROGAZIONE_ID + " DESC";
        Cursor data = db.rawQuery(query, null);
        data.moveToFirst();
        return data.getString(data.getColumnIndex("id"));
    }

    public void update(String newName, String id, String oldName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME_INTERROGAZIONE + " SET " + INTERROGAZIONE_NOME +
                " = '" + newName + "' WHERE " + INTERROGAZIONE_ID + " = '" + id + "'" +
                " AND " + INTERROGAZIONE_NOME + " = '" + oldName + "'";
        Log.d("SQL", "updateName: query: " + query);
        Log.d("SQL", "updateName: Setting name to " + newName);
        db.execSQL(query);
        db.close();
    }

    public void delate(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME_INTERROGAZIONE + " WHERE "
                + INTERROGAZIONE_ID + " = " + id;
        Log.d("SQL", "deleteName: query: " + query);
        db.execSQL(query);
        db.close();
    }

    public void delateQuestions(String idInterrogazione) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME_DOMANDA + " WHERE "
                + DOMANDA_ID_INTERROGAZIONE + " = " + idInterrogazione;
        db.execSQL(query);
        db.close();
        Log.d("SQL", "deleteName: query: " + query);
        Log.d("SQL", "deleteName: Deleting " + idInterrogazione + " from database.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_INTERROGAZIONE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_DOMANDA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SESSIONE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_DOMANDA_SESSIONE);
        onCreate(db);
        db.close();
    }
}
