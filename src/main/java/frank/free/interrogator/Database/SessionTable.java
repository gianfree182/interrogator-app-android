package frank.free.interrogator.Database;

public class SessionTable {

    private int id;
    private int id_interrogazione;
    private String timestampInizio;
    private String getTimestampFine;

    public SessionTable() {
    }

    public SessionTable(int id_interrogazione, String timestampInizio, String timestampFine) {
        this.id_interrogazione = id_interrogazione;
        this.timestampInizio = timestampInizio;
        this.getTimestampFine = timestampFine;
    }

    public SessionTable(int id_interrogazione, String timestampInizio) {
        this.id_interrogazione = id_interrogazione;
        this.timestampInizio = timestampInizio;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_interrogazione(int id_interrogazione) {
        this.id_interrogazione = id_interrogazione;
    }

    public void setTimestampInizio(String timestampInizio) {
        this.timestampInizio = timestampInizio;
    }

    public void setTimestampFine(String timestampFine) {
        this.timestampInizio = timestampFine;
    }

    public int getId() {
        return this.id;
    }

    public int getId_IdInterrogazione() {
        return this.id_interrogazione;
    }

    public String getTimeStampInizio() {
        return this.timestampInizio;
    }

    public String getTimestampFine() {
        return this.getTimestampFine;
    }
}
