package frank.free.interrogator.Database;

public class QuestionTable {

    private int id;
    private String domanda;
    private String risposta;
    private int idInterrogazione;

    public QuestionTable() {
    }

    public QuestionTable(String domanda, String risposta, int idInterrogazione) {
        this.domanda = domanda;
        this.risposta = risposta;
        this.idInterrogazione = idInterrogazione;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDomanda(String domanda) {
        this.domanda = domanda;
    }

    public void setRisposta(String risposta) {
        this.risposta = risposta;
    }

    public void setIdInterrogazione(int idInterrogazione) { this.idInterrogazione = idInterrogazione; }

    public int getId() {
        return this.id;
    }

    public String getDomanda() {
        return this.domanda;
    }

    public String getRisposta() {
        return this.risposta;
    }

    public int getIdInterrogazione() { return this.idInterrogazione; }
}
