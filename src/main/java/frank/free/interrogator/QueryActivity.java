package frank.free.interrogator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import frank.free.interrogator.Database.InterrogatorDbHelper;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;


public class QueryActivity extends AppCompatActivity {

    private TextView question;
    private TextView counter;
    private InterrogatorDbHelper db;
    private Button buttonShowAnswer;
    private Button buttonNextAnswer;
    private Button buttonStop;
    private Button buttonPosAns;
    private Button buttonNegAns;
    private Switch switchRandom;
    private Switch readQuestion;
    private String idInterrogazione;
    private String nameInterrogazione;
    private int stateNumberQuestion;
    private boolean random;
    private boolean read;
    private TextToSpeech mTTS;
    private ArrayList<Pair<String, String>> listQuestions;
    private int[] listQuestionLike;
    private int numberOfQuestion;
    private int numberOfPositiveAnswer;
    private boolean voted = false;
    private Chronometer chronometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);

        this.random = false;
        this.read = false;

        startQuestion();

        this.db = new InterrogatorDbHelper(this);
        this.question = (TextView) findViewById(R.id.textViewQuest);
        this.counter = (TextView) findViewById(R.id.counter);
        this.buttonShowAnswer = (Button) findViewById(R.id.buttonShowAnswer);
        this.buttonNextAnswer = (Button) findViewById(R.id.buttonNext);
        this.buttonStop = (Button) findViewById(R.id.buttonStop);
        this.switchRandom = (Switch) findViewById(R.id.random);
        this.readQuestion = (Switch) findViewById(R.id.switchRead);
        this.readQuestion.setChecked(false);
        this.buttonPosAns = (Button) findViewById(R.id.buttonPositive);
        this.buttonNegAns = (Button) findViewById(R.id.buttonNegative);

        Intent intent = getIntent();
        this.nameInterrogazione = intent.getStringExtra("interrogazione");
        this.idInterrogazione = intent.getStringExtra("id");

        getQuestions();
        checkIsReadLoud();

        this.chronometer = (Chronometer) findViewById(R.id.simpleChronometer);

        if(savedInstanceState != null){

            chronometer.setBase(savedInstanceState.getLong("ChronoTime"));
            chronometer.start();

        } else {
            chronometer.start();
        }


        this.switchRandom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    if(numberOfQuestion > 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(buttonView.getContext());

                        builder.setTitle("Attenzione!");
                        builder.setMessage("Cambiando in ordinamento random dovrai riniziare l'interrogazione, sei sicuro? ");
                        builder.setPositiveButton("Si, continua", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startQuestion();
                                getQuestionsRandom();
                            }
                        });
                        builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                switchRandom.setChecked(false);
                                random = false;
                            }
                        });
                        builder.show();
                    }
                    random = true;
                } else {
                    random = false;
                }
            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTTS.stop();
                onBackPressed();
            }
        });

        readQuestion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    speak();
                } else {
                    mTTS.stop();
                }
            }
        });

        this.buttonShowAnswer.setOnClickListener(v-> {
            mTTS.stop();
            if(this.buttonShowAnswer.getText().equals("mostra risposta")) {
                this.question.setText(this.listQuestions.get(stateNumberQuestion - 1).second.toString());
                this.buttonShowAnswer.setText("mostra domanda");
                if(readQuestion.isChecked()) {
                    speak();
                }
            } else {
                this.question.setText(this.listQuestions.get(stateNumberQuestion - 1).first.toString());
                this.buttonShowAnswer.setText("mostra risposta");
                if(readQuestion.isChecked()) {
                    speak();
                }
            }
        });

        this.buttonPosAns.setOnClickListener(v -> {
            if(this.listQuestionLike[stateNumberQuestion - 1] == 0) {
                this.listQuestionLike[stateNumberQuestion - 1] = 1;
                this.buttonPosAns.setBackgroundColor(Color.BLUE);
            } else if (this.listQuestionLike[stateNumberQuestion - 1] == -1) {
                this.listQuestionLike[stateNumberQuestion - 1] = 1;
                this.buttonPosAns.setBackgroundColor(Color.BLUE);
                int backgroundColor = ContextCompat.getColor(this, R.color.greenBut);
                this.buttonNegAns.setBackgroundColor(backgroundColor);
            } else {
                this.listQuestionLike[stateNumberQuestion - 1] = 0;
                int backgroundColor = ContextCompat.getColor(this, R.color.greenBut);
                this.buttonPosAns.setBackgroundColor(backgroundColor);
            }
        });

        this.buttonNegAns.setOnClickListener(v -> {
            if(this.listQuestionLike[stateNumberQuestion - 1] == 0) {
                this.listQuestionLike[stateNumberQuestion - 1] = -1;
                this.buttonNegAns.setBackgroundColor(Color.BLUE);
            } else if (this.listQuestionLike[stateNumberQuestion - 1] == 1) {
                this.listQuestionLike[stateNumberQuestion - 1] = -1;
                this.buttonNegAns.setBackgroundColor(Color.BLUE);
                int backgroundColor = ContextCompat.getColor(this, R.color.greenBut);
                this.buttonPosAns.setBackgroundColor(backgroundColor);
            } else {
                this.listQuestionLike[stateNumberQuestion - 1] = 0;
                int backgroundColor = ContextCompat.getColor(this, R.color.greenBut);
                this.buttonNegAns.setBackgroundColor(backgroundColor);
            }
        });

        this.buttonNextAnswer.setOnClickListener(v -> nextQuestion());

        this.counter.setText("Domanda: " + stateNumberQuestion + " / " + numberOfQuestion);

    }

    @Override
    public void onSaveInstanceState (Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putLong("ChronoTime", chronometer.getBase());
    }

    private void getQuestions() {
        Cursor data = this.db.selectDomandeFromId(idInterrogazione);
        while(data.moveToNext()) {
            listQuestions.add(new Pair<>(data.getString(1), data.getString(2)));
            numberOfQuestion++;
        }
        this.listQuestionLike = new int [numberOfQuestion];

        for(int i = 0; i < listQuestionLike.length; i++) {
            this.listQuestionLike[i] = 0;
        }
        this.question.setText(listQuestions.get(0).first);
        if(numberOfQuestion == 1) {
            this.buttonNextAnswer.setText("fine");
            Drawable img = this.getResources().getDrawable( R.drawable.flag_image);
            this.buttonNextAnswer.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
        }
    }

    private void nextQuestion() {
        resetAnswerButton();
        if (this.buttonNextAnswer.getText().equals("fine")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            for(int i = 0; i < listQuestionLike.length; i++) {
                if(listQuestionLike[i] == 1) {
                    numberOfPositiveAnswer++;
                }
            }

            chronometer.stop();

            if (numberOfQuestion == numberOfPositiveAnswer) {
                builder.setTitle("Grande!");
                builder.setIcon(R.drawable.positive_image);
                builder.setMessage("Hai risposto correttamente a tutte le risposte!");
            } else if(numberOfPositiveAnswer > 1) {
                builder.setTitle("Complimenti!");
                builder.setIcon(R.drawable.positive_image);
                builder.setMessage("Hai completato l'interrogazione con " + numberOfPositiveAnswer +
                        " risposte positive!");
            } else if (numberOfPositiveAnswer == 1) {
                builder.setTitle("Bravo!");
                builder.setIcon(R.drawable.positive_image);
                builder.setMessage("Hai completato l'interrogazione con " + numberOfPositiveAnswer +
                        " risposta positiva!");
            } else {
                builder.setTitle("Accipicchia!");
                builder.setIcon(R.drawable.negative_image);
                builder.setMessage("Non hai risposto positivamente a nessuna risposta!");
            }

            builder.setPositiveButton("Continua", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(QueryActivity.this, MainActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            stateNumberQuestion++;
            if(stateNumberQuestion == numberOfQuestion) {
                this.buttonNextAnswer.setText("fine");
                Drawable img = this.getResources().getDrawable( R.drawable.flag_image);
                this.buttonNextAnswer.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
            }
            this.question.setText(this.listQuestions.get(stateNumberQuestion - 1).first.toString());
            this.buttonShowAnswer.setText("mostra risposta");
            this.counter.setText("Domanda: " + stateNumberQuestion + " / " + numberOfQuestion);
            if(this.readQuestion.isChecked()) {
                speak();
            }
        }
    }

    private void checkIsReadLoud() {

        mTTS = new TextToSpeech(this, new TextToSpeech.OnInitListener(){
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTTS.setLanguage(Locale.ITALIAN);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Language not supported");
                    } else {
                        //read.setEnabled(true);  si potrebbe disabilitare lo switch se non compatibile
                    }
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            }
        });
    }

    private void speak() {
        String text = question.getText().toString();
        /*-float pitch = (float) mSeekBarPitch.getProgress() / 50;
        if (pitch < 0.1) pitch = 0.1f;
        float speed = (float) mSeekBarSpeed.getProgress() / 50;
        if (speed < 0.1) speed = 0.1f;

        mTTS.setPitch(pitch);
        mTTS.setSpeechRate(speed);*/

        mTTS.setPitch(0.7f);
        mTTS.setSpeechRate(0.8f);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTTS.speak(text, TextToSpeech.QUEUE_FLUSH,null,null);
        } else {
            mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }

    }

    private void startQuestion() {
        this.read = false;
        this.stateNumberQuestion = 1;
        this.numberOfQuestion = 0;
        this.numberOfPositiveAnswer = 0;
        this.listQuestions = new ArrayList<>();
    }

    private void getQuestionsRandom(){
        Cursor data = this.db.selectDomandeFromId(idInterrogazione);
        while(data.moveToNext()) {
            listQuestions.add(new Pair<>(data.getString(1), data.getString(2)));
            numberOfQuestion++;
        }
        this.listQuestionLike = new int [numberOfQuestion];
        for(int i = 0; i < listQuestionLike.length; i++) {
            this.listQuestionLike[i] = 0;
        }
        Collections.shuffle(listQuestions);
        this.question.setText(listQuestions.get(0).first);
        if(numberOfQuestion == 1) {
            this.buttonNextAnswer.setText("fine");
            Drawable img = this.getResources().getDrawable( R.drawable.flag_image);
            this.buttonNextAnswer.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
        }
    }

    @Override
    public void onBackPressed() {
        if(stateNumberQuestion > 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Attenzione!");
            builder.setIcon(R.drawable.excla_image);
            builder.setMessage("Vuoi interrompere l'interrogazione, sei sicuro? ");
            builder.setPositiveButton("Esci", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mTTS.stop();
                    QueryActivity.super.onBackPressed();
                }
            });
            builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            builder.show();
        } else {
            QueryActivity.super.onBackPressed();
        }

    }

    private void resetAnswerButton(){
        int backgroundColor = ContextCompat.getColor(this, R.color.greenBut);
        this.buttonPosAns.setBackgroundColor(backgroundColor);
        this.buttonNegAns.setBackgroundColor(backgroundColor);
    }

}
