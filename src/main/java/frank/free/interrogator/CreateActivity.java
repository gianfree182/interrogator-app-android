package frank.free.interrogator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import frank.free.interrogator.Database.InterrogatorDbHelper;
import frank.free.interrogator.Database.QuestionTable;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class CreateActivity extends AppCompatActivity {


    private InterrogatorDbHelper databaseSQL;
    private QuestionTable questionTable;
    private Button addQuestion;
    private Button cancel;
    private ArrayList<Pair<String, String>> listData;
    private ArrayList<String> listOfQuestion;
    private ListView listView;
    private ArrayAdapter<String> arrayNumberQuestadapter;
    private EditText name;
    private Button saveInDB;
    private Boolean addIsPressed;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        this.name = (EditText) findViewById(R.id.interrogazione);

        if(savedInstanceState != null) {
            name.setText(savedInstanceState.getString("Name"));
        }
        this.questionTable = new QuestionTable();
        this.databaseSQL = new InterrogatorDbHelper(this);
        this.listView = (ListView) findViewById(R.id.listview_question);
        this.addQuestion = (Button) findViewById(R.id.buttonAddQuestion);
        this.saveInDB = (Button) findViewById(R.id.salva);
        listData = new ArrayList<Pair<String,String>>();
        this.listOfQuestion = new ArrayList<>();
        this.addIsPressed = false;
        this.cancel = (Button) findViewById(R.id.annulla);

        this.cancel.setOnClickListener(v -> this.onBackPressed());

        this.arrayNumberQuestadapter = new ArrayAdapter<String>(this, R.layout.item_question_listview, R.id.textNumber, listOfQuestion);
        saveInDB.setOnClickListener(v->saveInDB());
        listView.setItemsCanFocus(true);
        listView.setAdapter(arrayNumberQuestadapter);
        addQuestion.setOnClickListener(v-> addQuestion());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("Name", this.name.toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedInstanceState.getString("Name");
    }

    public void saveInDB() {
        String stringName = name.getText().toString();

        if(TextUtils.isEmpty(stringName)) {
            name.setError("Inserire un nome");
            return;
        } else {
            this.databaseSQL.insert(this.name.getText().toString());
            String id = this.databaseSQL.getHighterId();
            Toast.makeText(this, "'" + stringName + "' salvata!", Toast.LENGTH_LONG).show();

            for(int i = 0; i < listData.size(); i++){
                View view = listView.getChildAt(i);
                EditText editTextDomanda = view.findViewById(R.id.editTextDomanda);
                EditText editTextRisposta = view.findViewById(R.id.editTextRisposta);
                if(!isEmpty(editTextDomanda) || !isEmpty(editTextRisposta)) {
                    this.databaseSQL.insertDomanda(editTextDomanda.getText().toString(), editTextRisposta.getText().toString(), Integer.parseInt(id));
                }
            }
            Intent intent = new Intent(CreateActivity.this, MainActivity.class);
            intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }

    private void addQuestion() {
        this.addIsPressed = true;
        listData.add(new Pair<>(" ", " "));
        listOfQuestion.add("#" + listData.size());
        arrayNumberQuestadapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if(!this.name.getText().toString().isEmpty() || addIsPressed) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Attenzione!");
            builder.setIcon(R.drawable.excla_image);
            builder.setMessage("Perderai tutto il lavoro che hai fatto, sei sicuro? ");
            builder.setPositiveButton("Esci", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    CreateActivity.super.onBackPressed();
                }
            });
            builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //CreateActivity.super.onBackPressed();
                    //rimani nella stessa activity
                }
            });
            builder.show();
        } else {
            CreateActivity.super.onBackPressed();
        }

    }

    public ArrayAdapter<String> getArrayNumberQuestadapter() {
        return arrayNumberQuestadapter;
    }

    public ListView getListView() {
        return listView;
    }

    public ArrayList<String> getListOfQuestion() {
        return listOfQuestion;
    }

    public ArrayList<Pair<String, String>> getListData() {
        return listData;
    }

    public InterrogatorDbHelper getDatabaseSQL() {
        return databaseSQL;
    }

    public QuestionTable getQuestionTable() {
        return questionTable;
    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

}
