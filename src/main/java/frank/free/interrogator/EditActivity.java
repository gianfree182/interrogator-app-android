package frank.free.interrogator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import androidx.appcompat.app.AppCompatActivity;
import frank.free.interrogator.Database.InterrogatorDbHelper;
import frank.free.interrogator.Database.QuestionTable;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class EditActivity extends AppCompatActivity {

    private EditText name;
    private InterrogatorDbHelper databaseSQL;
    private String idInterrogazione;
    private String nameInterrogazione;
    private ArrayList<Pair<String, String>> listQuestions;
    private int numberOfQuestion;
    private QuestionTable questionTable;
    private Button addQuestion;
    private ArrayList<Pair<String, String>> listData;
    private ArrayList<String> listOfQuestion;
    private ListView listView;
    private AdapterEditQuestion arrayNumberQuestadapter;
    private Button saveInDB;
    private Button cancel;
    private Button deleteAll;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        this.questionTable = new QuestionTable();
        this.listOfQuestion = new ArrayList<>();
        this.databaseSQL = new InterrogatorDbHelper(this);
        this.listView = (ListView) findViewById(R.id.listview_question_edit);
        this.addQuestion = (Button) findViewById(R.id.buttonAddQuestion);
        this.saveInDB = (Button) findViewById(R.id.salva);
        this.deleteAll = (Button) findViewById(R.id.deleteAll);
        listData = new ArrayList<Pair<String,String>>();

        Intent intent = getIntent();
        this.nameInterrogazione = intent.getStringExtra("interrogazione");
        this.idInterrogazione = intent.getStringExtra("id");
        saveInDB.setOnClickListener(v->saveInDB());
        listView.setItemsCanFocus(true);

        getQuestions();

        this.arrayNumberQuestadapter = new AdapterEditQuestion(this, listData, numberOfQuestion);
        listView.setAdapter(arrayNumberQuestadapter);
        addQuestion.setOnClickListener(v-> addQuestion());

        populateField();

        this.cancel = (Button) findViewById(R.id.annulla);

        this.cancel.setOnClickListener(v -> this.onBackPressed());

        this.deleteAll.setOnClickListener(v-> deleteAll());

    }

    private void populateField() {
        this.name = (EditText) findViewById(R.id.interrogazione);
        this.name.setText(this.nameInterrogazione);
    }

    private void getQuestions() {
        Cursor data = this.databaseSQL.selectDomandeFromId(idInterrogazione);
        while(data.moveToNext()) {
            listData.add(new Pair<>(data.getString(1), data.getString(2)));
            numberOfQuestion++;
            this.listOfQuestion.add("#" + this.listData.size());
        }
    }

    private void addQuestion() {
        databaseSQL.delateQuestions(this.idInterrogazione);
        for(int i = 0; i < listData.size(); i++){
            View view = listView.getChildAt(i);
            EditText editTextDomanda = view.findViewById(R.id.editTextDomanda);
            EditText editTextRisposta = view.findViewById(R.id.editTextRisposta);
            if(!isEmpty(editTextDomanda) || !isEmpty(editTextRisposta)) {
                if(isEmpty(editTextDomanda)) {
                    editTextDomanda.setText("");
                }
                if(isEmpty(editTextRisposta)) {
                    editTextRisposta.setText("");
                }
                this.databaseSQL.insertDomanda(editTextDomanda.getText().toString(), editTextRisposta.getText().toString(), Integer.parseInt(idInterrogazione));
            }
            if(i == listData.size() -1 && isEmpty(editTextDomanda) && isEmpty(editTextRisposta)) {
                Toast.makeText(this, "Piano! Puoi aggiungere un set per volta.", Toast.LENGTH_LONG).show();
            }
        }
        this.listData.clear();
        getQuestions();
        this.listData.add(new Pair<>(" ", " "));
        arrayNumberQuestadapter.notifyDataSetChanged();
        this.listView.smoothScrollToPosition(this.arrayNumberQuestadapter.getCount());
    }

    public void delete(View v){
        final int position = this.listView.getPositionForView((View) v.getParent());
        listData.remove(position);
        arrayNumberQuestadapter.notifyDataSetChanged();
    }

    public void saveInDB() {
        String stringName = name.getText().toString();

        if(TextUtils.isEmpty(stringName)) {
            name.setError("Inserire un nome");
            return;
        } else {
            Toast.makeText(this, "'" + stringName + "' modificata con successo!", Toast.LENGTH_LONG).show();

            databaseSQL.delateQuestions(this.idInterrogazione);

            databaseSQL.update(stringName, idInterrogazione, nameInterrogazione);


            for(int i = 0; i < listData.size(); i++){
                View view = listView.getChildAt(i);
                EditText editTextDomanda = view.findViewById(R.id.editTextDomanda);
                EditText editTextRisposta = view.findViewById(R.id.editTextRisposta);
                if(!isEmpty(editTextDomanda) || !isEmpty(editTextRisposta)) {
                    if(isEmpty(editTextDomanda)) {
                        editTextDomanda.setText("");
                    }
                    if(isEmpty(editTextRisposta)) {
                        editTextRisposta.setText("");
                    }
                    this.databaseSQL.insertDomanda(editTextDomanda.getText().toString(), editTextRisposta.getText().toString(), Integer.parseInt(idInterrogazione));
                }
            }
            Intent intent = new Intent(this, SelectEditActivity.class);
            intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }

    private void deleteAll() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Attenzione!");
        builder.setIcon(R.drawable.excla_image);
        builder.setMessage("Cosi facendo eliminerai l'interrogazione e tutte le sue domande, sei sicuro? ");
        builder.setPositiveButton("Sì, elimina", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                databaseSQL.delateQuestions(idInterrogazione);
                databaseSQL.delate(Integer.parseInt(idInterrogazione));
                Intent intent = new Intent(EditActivity.this, SelectEditActivity.class);
                intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Attenzione!");
        builder.setIcon(R.drawable.excla_image);
        builder.setMessage("Tutte le modifiche andranno perse, sei sicuro? ");
        builder.setPositiveButton("Esci", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                EditActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

}
